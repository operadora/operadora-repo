package org.develcorp.tickets.person.repository;

import org.develcorp.persistence.entity.person.Person;
import org.develcorp.persistence.entity.person.PersonType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepository extends JpaRepository <Person, Long> {

    List<Person> findByCpersonType(PersonType cpersonType);
}
