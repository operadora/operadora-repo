package org.develcorp.tickets.person.service;

import org.develcorp.persistence.entity.person.Person;

import java.util.List;

public interface PersonService {

    List<Person> listAllPerson();
    Person getPerson(Long cpersona);
    Person createPerson (Person person);
    Person updatePerson (Person person);
    Person deletePerson (Long cpersona);
}
