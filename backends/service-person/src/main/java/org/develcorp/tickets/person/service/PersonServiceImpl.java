package org.develcorp.tickets.person.service;

import lombok.RequiredArgsConstructor;
import org.develcorp.persistence.entity.person.Person;
import org.develcorp.tickets.person.repository.PersonRepository;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Override
    public List<Person> listAllPerson() {
        return personRepository.findAll();
    }

    @Override
    public Person getPerson(Long cpersona) {
        return personRepository.findById(cpersona).orElse(null);
    }

    @Override
    public Person createPerson(Person person) {
        person.setStatus("CREADO");
        person.setModifiedAt(new Date());

        return personRepository.save(person);
    }

    @Override
    public Person updatePerson(Person person) {
        Person personDB = getPerson(person.getCperson());

        if (null == personDB){
            return null;
        }

        personDB.setDni(person.getDni());
        personDB.setName(person.getName());
        personDB.setLastName(person.getLastName());
        personDB.setObservation(person.getObservation());
        personDB.setStatus("MODIFICADO");

        return personRepository.save(personDB);
    }

    @Override
    public Person deletePerson(Long cpersona) {
        Person personDB = getPerson(cpersona);

        if (null == personDB){
            return null;
        }

        personDB.setStatus("ELIMINADO");

        return personRepository.save(personDB);
    }
}