package org.develcorp.tickets.person.repository;

import org.develcorp.persistence.entity.person.PersonType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonTypeRepository extends JpaRepository<PersonType, Long> {
}
