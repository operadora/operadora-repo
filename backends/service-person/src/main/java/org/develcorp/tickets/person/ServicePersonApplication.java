package org.develcorp.tickets.person;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("org.develcorp.persistence")
public class ServicePersonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicePersonApplication.class, args);
	}

}