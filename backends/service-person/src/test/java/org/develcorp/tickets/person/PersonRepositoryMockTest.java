package org.develcorp.tickets.person;

import org.develcorp.persistence.entity.person.Person;
import org.develcorp.tickets.person.repository.PersonRepository;
import org.develcorp.tickets.person.repository.PersonTypeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PersonRepositoryMockTest {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PersonTypeRepository personTypeRepository;

    @Test
    void whenFindByPersonType_thenReturnListPerson(){
        /*PersonType personType = PersonType.builder()
                .cpersonType(1L)
                .description("COMPLETO")
                .build();
        personTypeRepository.save(personType);

        List<PersonType> founds = personTypeRepository.findAll();

        Assertions.assertThat(founds).hasSize(1);*/

        Person person1 = Person.builder()
                .name("BLANCA ANGELICA")
                .lastName("PINOS FIGUEROA")
                .dni("940954")
                .status("ACT")
                .observation("")
                .modifiedAt(new Date())
                .build();

        personRepository.save(person1);

        //List<Person> founds = personRepository.findByCpersonType(person1.getCpersonType());

        //Assertions.assertThat(founds).hasSize(2);
    }
}