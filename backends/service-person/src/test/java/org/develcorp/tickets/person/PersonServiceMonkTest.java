package org.develcorp.tickets.person;

import org.assertj.core.api.Assertions;
import org.develcorp.persistence.entity.person.Person;
import org.develcorp.tickets.person.repository.PersonRepository;
import org.develcorp.tickets.person.service.PersonService;
import org.develcorp.tickets.person.service.PersonServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;

@SpringBootTest
class PersonServiceMonkTest {

    @Mock
    private PersonRepository personRepository;

    private PersonService personService;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);

        personService = new PersonServiceImpl(personRepository);
        Person person = Person.builder()
                .cperson(10L)
                .dni("0384398433")
                .name("angea")
                .lastName("pind")
                .observation("")
                .modifiedAt(new Date())
                .status("INGRESADO")
                .build();

        Mockito.when(personRepository.findById(10L))
                .thenReturn(Optional.of(person));
    }

    @Test
    void whenValidGetID_ThenReturnPerson(){
        Person found = personService.getPerson(10L);
        Assertions.assertThat(found.getName()).isEqualTo("angea");
    }
}
