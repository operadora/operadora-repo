package org.develcorp.tools.common;

import java.text.MessageFormat;

public class DevelCorpException extends Throwable {
    public DevelCorpException(Exception e) {
        super(e);
    }

    public DevelCorpException(String message) {
        super(message);
    }

    public DevelCorpException(String message, Object ...params){
        super(MessageFormat.format(message, params));
    }

    public DevelCorpException(String message, Exception e) {
        super(message, e);
    }
}
