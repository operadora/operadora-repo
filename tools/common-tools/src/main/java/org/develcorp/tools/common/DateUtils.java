package org.develcorp.tools.common;

public class DateUtils {

    public static final String STR_DF_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String STR_DF_DD_MM_YYYY = "dd-MM-yyyy";
    public static final String STR_DF_YYYYMMDD = "yyyyMMdd";
}
