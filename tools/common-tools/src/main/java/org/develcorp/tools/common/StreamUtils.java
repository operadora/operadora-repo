package org.develcorp.tools.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StreamUtils {

    private static final Logger logger = Logger.getLogger(StreamUtils.class.getName());

    private static class SingletonHelper {
        private static final StreamUtils INSTANCE = new StreamUtils();
    }

    public static StreamUtils getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public void closeImputStreams(InputStream ...pIs) {
        for(InputStream is : pIs) {
            closeInputStream(is);
        }
    }

    private void closeInputStream(InputStream is) {
        if (is == null)
            return;

        try {
            is.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "ERROR CERRANDO STREAM", e);
        }
    }
}
