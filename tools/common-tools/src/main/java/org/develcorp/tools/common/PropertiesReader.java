package org.develcorp.tools.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

    private static final String PROPERTIES_FILE_EXTENSION = ".properties";
    private static final String DEFAULT_PROPERTIES_DIRECTORY = "/home/develcorp/config/properties/";

    private PropertiesReader() {}

    private static class SingletonHelper {
        private static final PropertiesReader INSTANCE = new PropertiesReader();
    }

    public static PropertiesReader getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public Properties getPropertiesFile(String propertiesName) throws DevelCorpException {
        FileInputStream fis = null;
        try {
            final String propertiesFilePath = getPropertiesFilePath(DEFAULT_PROPERTIES_DIRECTORY, propertiesName);
            fis = new FileInputStream(propertiesFilePath);
            Properties properties = new Properties();
            properties.load(fis);
            return properties;
        } catch (IOException e) {
            throw new DevelCorpException("ERROR INTENTANDO LEER ARCHIVO PROPERTIES {0} EN LA RUTA {1}",
                    propertiesName, DEFAULT_PROPERTIES_DIRECTORY);
        } finally {
            StreamUtils.getInstance().closeImputStreams(fis);
        }
    }

    private String getPropertiesFilePath(String pDirectory, String pPopertiesName) {
        final String directory = pDirectory.endsWith("/") ? pDirectory : pDirectory.concat("/") ;
        final String propertiesName = pPopertiesName.endsWith(PROPERTIES_FILE_EXTENSION) ?
                pPopertiesName : pPopertiesName.concat(PROPERTIES_FILE_EXTENSION);
        return String.format("%s%s", directory, propertiesName);
    }

}
