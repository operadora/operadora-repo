package org.develcorp.persistence.entity.person;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table (name = "TPERSONAS")
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class Person {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "cpersona")
    private Long cperson;

    private String dni;

    @Column (name = "nombres")
    private String name;

    @Column (name = "apellidos")
    private String lastName;

    @Column (name = "observacion")
    private String observation;

    @Column (name = "estado")
    private String status;

    @Column (name = "fmodificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
}