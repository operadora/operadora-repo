package org.develcorp.persistence.entity.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.develcorp.persistence.pk.person.PersonContactId;

import javax.persistence.*;

@Entity
@Table(name = "TCONTACTOSPERSONA")
@IdClass(PersonContactId.class)
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class PersonContact {

    @Id
    @Column(name = "cpersona")
    private Person cperson;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ccontactopersona")
    private Long cpersonContact;

    @Column (name = "descripcion")
    private String description;

    @Column (name = "tipo")
    private String type;
}