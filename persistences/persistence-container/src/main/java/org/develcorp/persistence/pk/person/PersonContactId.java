package org.develcorp.persistence.pk.person;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.develcorp.persistence.entity.person.Person;

import java.io.Serializable;

@AllArgsConstructor @NoArgsConstructor @EqualsAndHashCode
public class PersonContactId implements Serializable {

    private Person cPerson;
    private Long cpersonContact;

}