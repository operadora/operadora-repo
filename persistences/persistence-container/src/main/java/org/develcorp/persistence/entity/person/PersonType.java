package org.develcorp.persistence.entity.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table (name = "TTIPOSPERSONA")
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class PersonType {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "ctipopersona")
    private Long cpersonType;

    @Column (name = "descripcion")
    private String description;

}
